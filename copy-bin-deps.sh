#!/bin/bash
# Copy a binary and its shared library dependencies to a temporary directory
# The temporary directory must eventually replace the orignal one
# (full path must remain identical)
# Example:
# copy-bin.sh /tango Databaseds /tango-slim
# rm -rf /tango
# mv  /tango-slim /tango


PREFIX=$1
BINARY=$2
TARGET=$3

mkdir -p ${TARGET}/bin ${TARGET}/lib

# Copy binary
cp -p ${PREFIX}/bin/${BINARY} ${TARGET}/bin/

# Copy required libraries
cp -p $(ldd ${PREFIX}/bin/${BINARY} | grep "=> $PREFIX" | awk '{print $3}') ${TARGET}/lib/
