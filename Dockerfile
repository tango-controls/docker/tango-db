FROM condaforge/mambaforge as conda

ENV CPPTANGO_VERSION 9.4.1
ENV TANGO_DATABASE_VERSION 5.21
ENV TANGO_ADMIN_VERSION 1.18

RUN mamba create -y -p /tango \
    cpptango=$CPPTANGO_VERSION \
    tango-database=$TANGO_DATABASE_VERSION \
    tango-admin=$TANGO_ADMIN_VERSION \
  && mamba clean -afy

COPY copy-bin-deps.sh /

# Only keep required binaries and their shared library dependencies
RUN /copy-bin-deps.sh /tango Databaseds /tango-slim \
   &&  /copy-bin-deps.sh /tango tango_admin /tango-slim \
   && rm -rf /tango \
   && mv /tango-slim /tango

FROM debian:11-slim

LABEL maintainer="TANGO Controls Team <contact@tango-controls.org>"

RUN apt-get update \
  && apt-get install -y wait-for-it \
  && rm -rf /var/lib/apt/lists/*

COPY --from=conda /tango /tango

ENV PATH /tango/bin:$PATH
ENV ORB_PORT=10000
ENV TANGO_HOST=127.0.0.1:${ORB_PORT}

EXPOSE ${ORB_PORT}

RUN useradd -m tango
USER tango

CMD /usr/bin/wait-for-it $MYSQL_HOST --timeout=120 --strict -- \
    /tango/bin/Databaseds 2 -ORBendPoint giop:tcp:0.0.0.0:$ORB_PORT
