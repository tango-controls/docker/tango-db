# tango-db docker image

tango-db [docker] image to run [Tango Database server](https://gitlab.com/tango-controls/TangoDatabase).

The image includes:

- Tango Datadaseds
- tango_admin

## Usage

To use this image, you need a working instance of mysql with the tango tables created.

```console
docker run -it --rm --name tango-db \
  -e ORB_PORT=10000 \
  -e TANGO_HOST=127.0.0.1:10000 \
  -e MYSQL_HOST=mysql:3306 \
  -e MYSQL_USER=tango \
  -e MYSQL_PASSWORD=tango \
  -e MYSQL_DATABASE=tango \
  registry.gitlab.com/tango-controls/docker/tango-db
```

The recommended way is to use `docker-compose` to start the mysql container:

```yaml
version: '3.7'
services:
  mysql:
    image: registry.gitlab.com/tango-controls/docker/mysql:5
    environment:
     - MYSQL_ROOT_PASSWORD=root
  tango-db:
    image: registry.gitlab.com/tango-controls/docker/tango-db:5.16
    ports:
     - "10000:10000"
    environment:
     - TANGO_HOST=localhost:10000
     - MYSQL_HOST=mysql:3306
     - MYSQL_USER=tango
     - MYSQL_PASSWORD=tango
     - MYSQL_DATABASE=tango
    depends_on:
     - mysql
```

Run `docker-compose up`. The Tango Database server will be available on `localhost:10000`.

[docker]: https://www.docker.com
